package lang;

/**
 * Custom class that has the same name and package name as default Integer.
 */
public class Integer {
    static {
        System.out.println("Loaded!");
    }

    @Override
    public String toString() {
        return "my integer";
    }
}



