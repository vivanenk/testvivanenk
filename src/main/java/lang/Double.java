package lang;

/**
 * Custom class that duplicates Double and has main method.
 */
public class Double {

    static {
        System.out.println("Loaded!");
    }
    
    public static void main(String[] args) {
        Double d = new Double();
        System.out.println(d.getClass().getClassLoader());
    }
}


