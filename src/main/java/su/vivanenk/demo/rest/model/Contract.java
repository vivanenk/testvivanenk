package su.vivanenk.demo.rest.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@RequiredArgsConstructor
@Getter
@Setter
public class Contract {
    public final BigDecimal price;
    public final Customer customer;
    @Setter
    public  Tariff tariff;

    @Override
    public String toString(){
       return "Contract with price " + price;
    }




}
