package su.vivanenk.demo.rest.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@Getter
public class Tariff {
    public final LocalDate startDate;
    @Setter
    public List<Contract> contracts;
}
