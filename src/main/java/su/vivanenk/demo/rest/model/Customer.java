package su.vivanenk.demo.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Getter
public class Customer {
    @JsonProperty("name")
    @Setter
    public  String name;
    @Setter
    public  LocalDate birthday;
    public Long id;
    @Setter
    public List<Contract> contracts;

    @Override
    public String toString(){
        return String.format("Customer object; name=%1$s,birthday= %2$s", name, birthday);
    }

    public static Customer getInstance(String name, BigDecimal contractPrice){
        Customer customer = new Customer();
        Contract contract = new Contract(BigDecimal.valueOf(15.7).add(contractPrice) , customer);
        customer.setContracts(Stream.of(contract).collect(Collectors.toList()));

        Tariff tariff = new Tariff(LocalDate.now());
        contract.setTariff(tariff);
        return customer;
    }
}
