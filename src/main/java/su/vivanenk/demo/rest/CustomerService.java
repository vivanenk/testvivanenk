package su.vivanenk.demo.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import su.vivanenk.demo.data.entity.CustomerEntity;
import su.vivanenk.demo.data.repository.CustomerRepository;
import su.vivanenk.demo.mapping.CustomerMapper;
import su.vivanenk.demo.rest.model.Contract;
import su.vivanenk.demo.rest.model.Customer;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;

    private final CustomerMapper customerMapper;

    public List<Contract> searchContracts(){
        return Arrays.asList(new Contract(BigDecimal.valueOf(1), Customer.getInstance("test1", BigDecimal.valueOf(1))),
                new Contract(BigDecimal.valueOf(2), Customer.getInstance("test2", BigDecimal.valueOf(2))));
    }

    public Customer createCustomer(Customer customer){
        CustomerEntity customerEntity = customerMapper.map(customer);
        customerEntity.setName(customer.getName());
        customerEntity.setBirthday(customer.getBirthday());
        customerRepository.save(customerEntity);
        return customer;
    }
}
