package su.vivanenk.demo.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import su.vivanenk.demo.rest.model.Contract;
import su.vivanenk.demo.rest.model.Customer;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
public class CustomerRestController {
    private final CustomerService service;

    @RequestMapping(
            value = {"demo/getContracts"},
            produces = {"application/json"},
            method = {RequestMethod.GET})
    public ResponseEntity<List<Contract>> getContractsOfCustomers(long customerId){

        List <Contract> result = service.searchContracts();

        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @RequestMapping(value = "demo/updateCustomer",
            method = RequestMethod.PUT,
            produces = "application/json")
    public ResponseEntity<Void> updateCustomer(Customer customer){
        return  ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "demo/createCustomer", headers = {
                    "content-type=application/json" },
    produces = "application/json", consumes = { "application/json" },
    method = {RequestMethod.POST})
    public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {
        service.createCustomer(customer);
        log.info("Customer is created!");
        return ResponseEntity.status(HttpStatus.OK).body(customer);
    }

    @ExceptionHandler(value={Exception.class})
    private ResponseEntity<Object> handleException(Exception ex){
        log.error("error happened during the rest call", ex);
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
