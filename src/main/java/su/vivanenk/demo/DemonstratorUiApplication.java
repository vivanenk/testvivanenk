package su.vivanenk.demo;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
public class DemonstratorUiApplication {

	private DataSourceProperties datasourceProperties;

	public static void main(String[] args) {
		SpringApplication.run(DemonstratorUiApplication.class, args);
	}


	@Bean
	public Flyway flyway(){
		Flyway flyway = Flyway.configure()
				.dataSource(datasourceProperties.getUrl(),
						datasourceProperties.getUsername(), datasourceProperties.getPassword())
				.baselineOnMigrate(true)
				.load();
		flyway.migrate();
		return flyway;
	}

	@Autowired
	public void setDataSourceProperties(DataSourceProperties datasourceProperties) {
		this.datasourceProperties = datasourceProperties;
	}

}
