package su.vivanenk.demo.cache;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.support.AopUtils;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class CacheHandler<A, C extends Cloneable> implements MethodInterceptor {

    Map<A, C> cache = new ConcurrentHashMap<>();

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Method method = invocation.getMethod();
        Class<?> targetClass = AopUtils.getTargetClass(invocation.getThis());
        if (method.isAnnotationPresent(CustomCachePut.class)) {
          //  cache.putIfAbsent(invocation.getArguments()[0], invocation.getArguments()[1].clone());

        } else if (method.isAnnotationPresent(CustomCacheGet.class)) {
            return cache.get(invocation.getArguments()[0]);
        }
        return null;
    }
}
