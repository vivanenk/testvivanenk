package su.vivanenk.demo.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class CustomCache <K, V extends Cloneable> {

    private final Map<K,V> cache = new ConcurrentHashMap<>();

//    private final Map<K,V> innerCache;
//
//    public CustomCache (Map<K,V> map){
//        innerCache = new ConcurrentHashMap<>(map);
//    }

    public void addCache(K key, V value){
        cache.putIfAbsent(key, value);
    }

    public V getCache(K key){
        V v = cache.get(key);
        return v;
    }

    public void clearItem(K key){
        cache.remove(key);
    }
    public void clear(){
        cache.clear();
    }
}
