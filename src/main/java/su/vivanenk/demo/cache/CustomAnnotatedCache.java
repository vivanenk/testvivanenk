package su.vivanenk.demo.cache;

import su.vivanenk.demo.cache.CustomCacheGet;
import su.vivanenk.demo.cache.CustomCachePut;

public interface CustomAnnotatedCache {

    @CustomCacheGet
    public String getValue(String key);
    
    @CustomCachePut
    public void setValue(String key, String data);
}
