package su.vivanenk.demo.cache;

import java.util.Map;
import java.util.concurrent.*;
import java.util.function.Function;

public class CustomConcurrentCache<A, V> {
    private Map<A, Future<V>> cache = new ConcurrentHashMap<>();
    private Function<A, V> dataProvider;

    public CustomConcurrentCache(Function<A, V> dataProvider){
        this.dataProvider = dataProvider;
    }

    public V compute(final A arg) throws ExecutionException, InterruptedException {
        Future<V> future = cache.get(arg);
        if (future == null){
            Callable<V> task = () -> dataProvider.apply(arg);
            Future<V> vFuture = Executors.newSingleThreadExecutor().submit(task);
            cache.put(arg, vFuture);
            return vFuture.get();
        }
        return future.get();
    }


}
