package su.vivanenk.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import su.vivanenk.demo.cache.CustomConcurrentCache;
import su.vivanenk.demo.cache.CustomAnnotatedCache;

import java.util.concurrent.ExecutionException;
import java.util.function.Function;

@Controller
@Slf4j
public class DemoController implements CustomAnnotatedCache {

    private final Function<String, String> doCache = (name) -> name + "_concurrent_cached";

    private final CustomConcurrentCache<String, String> concurrentCache = new CustomConcurrentCache<>(doCache);

    @GetMapping("/demo")
    public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name,
        Model model) throws ExecutionException, InterruptedException {
        setValue("demo", name);
        log.info("Perform view rendering with parameter {}", name);

        log.info("from annotated cache {}", getValue(name));
        log.info("from concurrent cache {}", concurrentCache.compute(name));
        model.addAttribute("name", name);
        return "demo";
    }


    @Override
    public String getValue(String key) {
        return null;
    }

    @Override
    public void setValue(String key, String data) {
    }
}
