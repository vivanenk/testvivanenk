package su.vivanenk.demo.mapping;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import su.vivanenk.demo.data.entity.CustomerEntity;
import su.vivanenk.demo.rest.model.Customer;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

   // @Mapping(target = "contractEntities", ignore = true)
    CustomerEntity map (Customer customer);
}
