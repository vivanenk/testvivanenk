package su.vivanenk.demo.data.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "Customer")
@Data
@NoArgsConstructor
public class CustomerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private LocalDate birthday;

    @OneToMany (mappedBy = "customerEntity", cascade = CascadeType.ALL)
    private List<ContractEntity> contractEntities;



}
