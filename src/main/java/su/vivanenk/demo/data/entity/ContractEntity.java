package su.vivanenk.demo.data.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "Contract")
@Data
public class ContractEntity {
    @Id
    private Long id;

    private String name;

    @ManyToOne(targetEntity = CustomerEntity.class, cascade = CascadeType.ALL)
    @JoinColumn(name= "Customer")
    private CustomerEntity customerEntity;

}