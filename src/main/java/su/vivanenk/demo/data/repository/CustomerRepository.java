package su.vivanenk.demo.data.repository;

import org.springframework.data.repository.CrudRepository;
import su.vivanenk.demo.data.entity.CustomerEntity;

public interface CustomerRepository extends CrudRepository<CustomerEntity, Long> {
}
