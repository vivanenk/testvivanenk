package su.vivanenk.ocp;

import lombok.extern.slf4j.Slf4j;
import su.vivanenk.ocp.cocurrency.cyclicbarrier.LeonPenManager;
import su.vivanenk.ocp.cocurrency.forkjoin.WeightAnimalAction;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@Slf4j
public class Chpt7Concurrency {

    private static final Collection<Object> objects = Collections.synchronizedCollection(new ArrayList<>());
    private AtomicInteger atomicCount = new AtomicInteger(0);
    private static int count = 0;

    public synchronized int incrCount() {
        System.out.println( ++count);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return count;
    }

    private Map<String, Object> zooFood = new ConcurrentHashMap<>();

    private void incrementAndReport() {

        // log.info("count is {}", atomicCount.incrementAndGet());
        //  log.info("count is {}", incrCount());

        System.out.println("count is " +incrCount());
//        synchronized (atomicCount) {
//            System.out.print(atomicCount.incrementAndGet() + " ");
//        }
        //  System.out.print((++count) + " ");
    }

    ;

    public static final Runnable PRINT_ITERATION = () -> {
        for (int i = 0; i < 5; i++) {
            log.info(" inside runable thread i={}", i);
        }
    };
    public static final Callable<Integer> CALCULATE_ITERATION = () -> {
        int b = 0;
        for (int i = 0; i < 5; i++) {
            log.info(" inside callable thread i={}", i);
            b += i;
        }
        return b;
    };

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        List<Object> objects = Collections.synchronizedList(new ArrayList<>());


        Stream.of("test").parallel().parallel();


        // Chpt7Concurrency.testExecutorService();
        Chpt7Concurrency.testThreadPool();
        // Chpt7Concurrency.testForkJoin();
        //  Chpt7Concurrency.testCyclicBarrier();
    }

    public static void testCyclicBarrier() {
        LeonPenManager manager = new LeonPenManager();
        ExecutorService executorService = null;
        try {
            executorService = Executors.newFixedThreadPool(4);
            CyclicBarrier cb1 = new CyclicBarrier(4, () -> log.info("cage is empty"));
            CyclicBarrier cb2 = new CyclicBarrier(4);
            for (int i = 0; i < 4; i++) {
                executorService.submit(manager::manageWithoutBarrier);
//                    executorService.submit(() -> {
//                        try {
//                            manager.manageWithBarirer(cb1, cb2);
//                        } catch (BrokenBarrierException| InterruptedException ex) {
//                            log.error("concurrency exception ", ex);
//                        }
//                    });
            }
        } finally {
            if (executorService != null) executorService.shutdown();
        }
    }

    public static void testForkJoin() {
        Double[] weights = new Double[10];
        ForkJoinTask<?> weightAnimalAction = new WeightAnimalAction(weights, 0, weights.length);
        ForkJoinPool pool = new ForkJoinPool();
        pool.invoke(weightAnimalAction);
        Stream.of(weights).forEach(i -> log.info("results : {}", i));
        // log.info("results : {}", weights);
    }

    public static void testThreadPool() {
        ExecutorService executorService = null;
        Chpt7Concurrency concurrencyChptTest = new Chpt7Concurrency();
        concurrencyChptTest.zooFood.keySet().parallelStream();
        try {
            executorService = Executors.newFixedThreadPool(20);
            for (int i = 0; i < 10; i++) {
                executorService.submit(() -> concurrencyChptTest.incrementAndReport());
            }
        } finally {
            if (executorService != null) executorService.shutdown();
        }

    }

    public static void testExecutorService() throws ExecutionException, InterruptedException {
        log.info("start");
        ExecutorService service = null;
        try {

            service = Executors.newSingleThreadExecutor();

            service.execute(() -> log.info("inside separate thread"));

            service.execute(PRINT_ITERATION);
//            Future<?> submitIteration  = service.submit(PRINT_ITERATION);
            Future<Integer> calculationFuture = service.submit(CALCULATE_ITERATION);

            //  log.info("callable result is {}", calculationFuture.get());
            log.info("finish");
        } finally {
            if (service != null) {
                log.info("in the finally.  ");
                // service.shutdown();
                log.info("shout down now {} tasks not started", service.shutdownNow().size());
            }
        }


    }
}
