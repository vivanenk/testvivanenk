package su.vivanenk.ocp;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.CyclicBarrier;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.thymeleaf.util.StringUtils;
import su.vivanenk.demo.rest.model.Contract;
import su.vivanenk.demo.rest.model.Customer;

/**
 * Stream is sequence of data.
 * Stream is lazy evaluated, it means its intermediate operations works when terminate operation performed. (239 p.)
 * Stream operations - operation can be occurred on pipeline on they are performing in queue and never perform again
 * Types of operations:
 * - Source operations; can create finite Stream (Stream.of(...)) or infinite stream (Stream.generate, Stream.iterate)
 * - Intermediate operations (Return valid Stream type, can be used several times)
 * - Terminate operations (Uses stream pipeline).
 * Reduction is a special type of terminal operation when ALL content of the stream combined in single object or primitive.
 * e.g. count, collect, reduce, min/max
 *  Why  allMatch(), noneMatch() are not reductions ? (p. 244)
 * not reduction: forEach,  anyMatch, firstMatch
 * ...
 * Page 252
 * Takes two first name alphabetically only with 4 character long
 * List<String> list = Arrays.asList("Toby", "Anna", "Leroy", "Alex");
 *
 * list.stream()
 *    .filter(i-> i.length() == 4)
 *    .sorted()
 *    .limit(2)
 *    .forEach(System.out::println);
 *
 * In this example sorted is a special one !!!
 * [source] ---element--->filter(check element) ---element--->
 *          ---> sorted(COLLECTS ALL ELEMENTS passed from filer) ---element --->
 *          -->limit(count) ---element --->forEach(print)
 */
@Slf4j
@NoArgsConstructor
public class Chpt4Streams {

    public static void main (String [] args) throws Exception{

        Chpt4Streams ddd = new Chpt4Streams();
       // Chpt4Streams.testStreamLoop();
     //   Chpt4Streams.testCollectToMap();
       // Chpt4Streams.checkChapter4();
       // Chpt4Streams.testGroupBy();
      //  Chpt4Streams.currentTest();
     //   Chpt4Streams.testReductions();

        double principle = 100;
        int interestrate = 5;
        double amount = compute(principle, x -> x * interestrate);
        System.out.println(amount);
        }

    public static double compute(double base, Function<Integer, Integer> func){
        return func.apply((int)base);
    }

    private static void testReductions() {
        // Leroy is out of sorting
        List<String> list = Arrays.asList("Toby", "Anna", "Leroy", "Alex");
        list.stream()
                .filter(i -> i.length() == 4)
                .sorted()
                .limit(2)
                .forEach(System.out::println);
        // this is no good way sinse in sorted all elements
        list.stream()
                .sorted()
                .filter(i -> i.length() == 4)
                .limit(2)
                .forEach(System.out::println);
    }

    public static void checkChapter4(){
        CyclicBarrier cb = new CyclicBarrier(4);

        //what is reductions
        Stream<String> stream1 = Stream.iterate("", s -> s + "1");
        Stream<String> x1 = stream1.limit(2).map(x -> x + "2");
        System.out.println(x1);
        System.out.println(x1.collect(Collectors.toList()));

        Predicate <? super String> predicate = s -> s.startsWith("g");
        Stream<String> generate = Stream.iterate("", s -> "g" +s);
       // Stream<String> generate = Stream.generate(() -> "growl! ");
        boolean b1 = generate.anyMatch(predicate);
     //   boolean b2 = generate.allMatch(predicate);
        System.out.println(b1 + " " );

        IntStream empty = IntStream.empty();
        empty.findFirst().ifPresent(System.out::println);
     //   empty.findFirst().ifPresent(e -> log.info("first element is {}", e));
       /// log.info("empty int stream result {}", empty.average().ifPresent(empty.count()).getAsDouble());

//        Stream.generate(() -> "1")
//                .filter(i-> i.length() > 1)
//                .forEach(System.out::println);
  //              .limit(10)
//                .peek(System.out::println);

        System.out.println(Stream.iterate(1, x->++x)
                .limit(5)
                .map(x->"" + x).collect(Collectors.joining("")));

        Supplier<String> xxx = String::new;
        BiConsumer<String,String> y = (a,b) -> System.out.println();
        UnaryOperator<String> z = a -> a + a;
        Function<String,String> w = v -> v + v;

        List<Integer> lst1 = Arrays.asList(1,2,3);
        List<Integer> lst2 = Arrays.asList();
        Stream.of(lst1, lst2).flatMap(Collection::stream).map(xx->xx+1).forEach(System.out::println);


        Stream<Integer> is = Stream.of(1);
        IntStream intStream =  is.mapToInt(x -> x);
       // DoubleStream doubleStream = is.mapToDouble(x -> x);
      // Stream<Integer> cc = is.mapToInt(x -> x);
        intStream.forEach(System.out::println);

        Map<Boolean, Map<String, Integer>> collect = Stream.of(lst1, lst2).flatMap(Collection::stream)
                .collect(Collectors.partitioningBy(l -> l == 1, Collectors.toMap(Object::toString, k -> k)));

        DoubleStream ds= DoubleStream.of(1.2, 2.4);

        ds.peek(System.out::println).filter(x -> x>2).count();

        Stream<String> stringStream = Stream.of("1.2", "2.4");

        stringStream.peek(s-> System.out.println(s.length())).filter(s->s.length()>2).count(); // peek do the loop ?


    }

    public static void testGroupBy(){
        Customer customer1 = Customer.getInstance("vasia", BigDecimal.valueOf(1));
        Customer customer2 = Customer.getInstance("petia", BigDecimal.valueOf(2));
        Customer customer3 = Customer.getInstance("vova", BigDecimal.valueOf(3));
        Customer customer4 = Customer.getInstance("vasia", BigDecimal.valueOf(3));

        TreeMap<String, Set<Customer>> customers = Stream.of(customer1, customer2, customer3)
                .collect(Collectors.groupingBy(Customer::getName, TreeMap::new, Collectors.toSet()));

        log.info("Customers {}", customers);

        HashMap<String, Set<LocalDate>> customersByDate = Stream.of(customer1, customer2, customer3)
                .collect(Collectors.groupingBy(Customer::getName, HashMap::new, Collectors.mapping(Customer::getBirthday, Collectors.toSet())));

        log.info("Customers by dates {}", customersByDate);

        HashMap<String, Set<Contract>> customerWithContracts = Stream.of(customer1, customer2, customer3)
                .collect(Collectors.groupingBy(Customer::getName, HashMap::new,
                        Collectors.flatMapping(c -> c.getContracts().stream(), Collectors.toSet())));

        log.info("Customers with contracts {}", customerWithContracts);

        Map<Integer, List<Contract>> collect = Stream.of(customer1, customer2, customer3)
                .collect(Collectors.toMap(c -> c.getName().length(), Customer::getContracts, (s, a) -> {s.addAll(a); return s;}));

        log.info("Customers by length {}", collect);

        HashMap<String, Set<Customer>> customerByName = Stream.of(customer1, customer2, customer3, customer4)
                .collect(Collectors.groupingBy(Customer::getName, HashMap::new,
                        Collectors.mapping(Function.identity(), Collectors.toSet())));
        log.info("Customers by name {}", customerByName);
    }

    public static void testCollectToMap(){
       Stream<String> animalsStream = Stream.of("lion", "tiger", "bears", "tiger");
     //   Map<Integer, String> map1 = animalsStream.collect(Collectors.toMap( String::length, k -> k));
        Map<Integer, String> map2 = animalsStream.collect(Collectors.toMap(String::length, k -> k, (s1, s2) -> s1 + s2));
    //    System.out.println(map1);
        System.out.println(map2);
    }

    public static void testStreamLoop(){
        System.out.println("test stream loop");
        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
         integerStream
                .filter(i -> i <10)
                .peek( System.out::println)
                .map(StringUtils::toString).collect(Collectors.toSet());
    }

    private void testStreamOrder(){
        Stream<String> stream = Stream.of("one", "two", "three");
        stream.sorted(Comparator.reverseOrder()).peek(log::debug).forEach(System.out::print);
       // stream.sorted(Comparator::reverseOrder).forEach(System.out::print); /// ?????
        stream.sorted(Comparator.comparingInt(String::length)).forEach(System.out::print);
        stream.sorted((a,b)->a.length()-b.length()).forEach(System.out::println);
    }

}
