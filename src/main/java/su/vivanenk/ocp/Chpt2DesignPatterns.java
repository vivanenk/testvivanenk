package su.vivanenk.ocp;

import lombok.extern.slf4j.Slf4j;
import su.vivanenk.ocp.designpatterns.Travel;
import su.vivanenk.ocp.designpatterns.TravelToBelarus;
import su.vivanenk.ocp.designpatterns.TravelToPatagonia;

import java.security.AccessController;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Shows that:
 * - default methods in the interface can be not overrided in the class
 * - static method in the interface can be run on instance variable of the class only this variable has class type
 * - cast from subtype to supertype is automatic
 *
 * Nested class is the class defined within another class.
 * Nested class which is not static is Inner class.
 * static classes is used when :
 *  - it is not needed to have link to not static objects of outer class
 */

@Slf4j
public class Chpt2DesignPatterns {



    private static String STAT_TEST= "statOuterTest";
    private String testInOuter= "test in outer";

    NestedClassTest n = new Chpt2DesignPatterns().new NestedClassTest();

    public static void main(String[] args) {



        Travel travel = new TravelToPatagonia();
        travel.getVacation();
        Travel.closeHouse();
        TravelToPatagonia.closeHouse();
        TravelToBelarus bTravel = new TravelToBelarus();
        bTravel.getVacation();
        bTravel.closeHouse();
     //   travel.closeHouse();
        Travel travel2 = new TravelToPatagonia();
        log.info("country {}", travel2.getCountry());
        travel2= bTravel;
        log.info("country {}", travel2.getCountry());
    //    bTravel = travel2;
        log.info("country {}", bTravel.getCountry());
    }

    private void innerClassTest() {
        class WildCards {

          //  enum EnumB{ B, BB, BBB }
            private String test = "testinner";
           private static final String STAT_INNER = "stat inner";
            public void printList(List<?> list) {
                System.out.println(list);
            }
            private String returnTest(){
                return test +testInOuter + STAT_TEST;
            }
        }
        WildCards wildCards = new WildCards();
        List<String> stringList = new ArrayList<>();
        stringList.add("one");
        wildCards.printList(stringList);
        System.out.println(wildCards.test);

        List<Integer> intList = new ArrayList<>();
        intList.add(1);
        wildCards.printList(intList);
    }

    private void nestedClassTest(){

        NestedClassTest classTest = new NestedClassTest();
        System.out.println(classTest.test);
        System.out.println(classTest.getTest());

    }

    private class NestedClassTest {

        int a =5;
        int b=0;

        int c  = a/b;


        private String test= "testNested";
     //   private static  String STAT_NSTED = "stat nested";

        private String returnTest(){
            //return test  +testInOuter;
            return test + STAT_TEST;// +testInOuter; // Doesn't compile
        }

        public String getTest() {
            return "from get is " + test ;
        }
    }



    public interface TestInt{
       // String test;
          void test();
    }

}
