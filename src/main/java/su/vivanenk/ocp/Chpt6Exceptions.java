package su.vivanenk.ocp;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

@Slf4j
public class Chpt6Exceptions {

    public static void main(String[] args) {
//        try {
//            Chpt6Exceptions.testCheckedExceptions();
//        } catch (NoSuchFileException e) {
//            log.error("NoSuchFileException exception", e);
////        }
//        try {
//            Chpt6Exceptions.tryWithResources2();
//        } catch (IOException e) {
//            log.error("inside main catch", e);
//        }
        try {
            Chpt6Exceptions.tryWithResources2();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void tryWithResources2() throws IOException {
        try (MyClosableResource myClosableResource = new MyClosableResource()) {
            log.info("inside safe code");
            throw new FileNotFoundException("file not found");
            // ...

        } finally {
            log.info("inside finally");
            throw new RuntimeException("hahaha in the finnaly ");
        }
    }

    public static void tryWithResources() {
        ResourceBundle zooResource = ResourceBundle.getBundle("Zoo");
        try (BufferedReader r = Files.newBufferedReader(Paths.get(zooResource.getString("filepath")));
             BufferedWriter w = Files.newBufferedWriter(Paths.get(zooResource.getString("filepath")))) {
            w.write(r.readLine());
        } catch (IOException ex) {
            log.error("io exception ", ex);
        } finally {
            log.info("finally");
        }
    }

    public static void testCheckedExceptions() throws DateTimeException, NoSuchFileException {
        ResourceBundle zooResource = ResourceBundle.getBundle("Zoo");
        try {
            //      LocalDateTime.parse(zooResource.getString("name"));
            Files.readAllBytes(Paths.get(zooResource.getString("filepath")));
        }
//        catch (NoSuchFileException ex2){
//            log.error("NoSuchFileException exception", ex2);
//        }
        catch (IOException ex) {
            log.error("io exception", ex);
        } catch (DateTimeException ex3) {
            log.error("datetime exception", ex3);
        }
    }

    public static void testCheckedExceptions2() {
        ResourceBundle zooResource = ResourceBundle.getBundle("Zoo");
        try {
            LocalDateTime.parse(zooResource.getString("name"));
            Files.readAllBytes(Paths.get(zooResource.getString("filepath")));
        } catch (IOException | DateTimeException ex) {
            log.error("exception", ex);
        }
    }

//    public String getDataFromDatabase() throws SQLException {
//        boolean assert1 = false;
//
//        throw new Exception();
//    }

    public static String getDataFromDatabase2() throws FileNotFoundException, SQLException, Exception {
        try {
            getDataFromDatabase2();
        } catch (Exception e) {
            e = new RuntimeException();
            throw e;
        } finally {
            log.info("in the finally");
        }
        return null;
    }


    static class MyClosableResource implements Closeable {
        @Override
        public void close() throws IOException {
            log.info("closing");
            throw new IOException("io in the close");
        }
    }
}
