package su.vivanenk.ocp.cocurrency.forkjoin;

import lombok.extern.slf4j.Slf4j;

import java.util.Random;
import java.util.concurrent.RecursiveAction;

@Slf4j
public class WeightAnimalAction extends RecursiveAction {

    private Double[] weights;
    private int start;
    private int end;


    public WeightAnimalAction(Double[] weights, int start, int end) {
        this.weights = weights;
        this.start = start;
        this.end = end;
    }

    @Override
    protected void compute() {
        if (end - start <= 3) {
            for (int i = start; i < end; i++) {
                 weights[i] = (double)(new Random()).nextInt(15)/100;
                log.info("animal {} weighed " , i);
            }
        } else {
            int middle = start + ((end-start)/2);
            log.info("start {} , middle {} , end {}", start, middle, end);
            invokeAll(new WeightAnimalAction(this.weights, start, middle),
                    new WeightAnimalAction(this.weights, middle, end));
        }
    }
}

