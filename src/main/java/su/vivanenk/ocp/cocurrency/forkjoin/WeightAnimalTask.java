package su.vivanenk.ocp.cocurrency.forkjoin;

import lombok.extern.slf4j.Slf4j;

import java.util.Random;
import java.util.concurrent.RecursiveTask;

@Slf4j
public class WeightAnimalTask extends RecursiveTask {

    private Double[] weights;
    private int start;
    private int end;


    public WeightAnimalTask(Double[] weights, int start, int end) {
        this.weights = weights;
        this.start = start;
        this.end = end;
    }

    @Override
    protected Double compute() {
        if (end - start <= 3) {
            double sum = 0;
            for (int i = start; i < end; i++) {
                weights[i] = (double) (new Random()).nextInt(15) / 100;
                log.info("animal {} weighed ", i);
                sum += weights[i];
            }
            return sum;
        } else {
            int middle = start + ((end - start) / 2);
            log.info("start {} , middle {} , end {}", start, middle, end);
            RecursiveTask<Double> otherTask = new WeightAnimalTask(weights, start, middle);
            otherTask.fork();
            return (new WeightAnimalTask(weights, middle, end)).compute() + otherTask.join();
        }
    }
}

