package su.vivanenk.ocp.cocurrency.cyclicbarrier;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

@Slf4j
public class LeonPenManager {

    private void clearPen(){log.info("clear pen");}
    private void removeAnimals(){log.info("remove animals");}
    private void addAnimals(){log.info("add animals");}

    public void manageWithoutBarrier(){
        removeAnimals();
        clearPen();
        addAnimals();
    }

    public void manageWithBarrier(CyclicBarrier cb1, CyclicBarrier cb2 ) throws BrokenBarrierException, InterruptedException {
        removeAnimals();
        cb1.await();
        clearPen();
        cb2.await();
        addAnimals();
    }
}
