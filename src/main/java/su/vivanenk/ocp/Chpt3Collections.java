package su.vivanenk.ocp;

import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Shows that:
 * List - order by insertion. allow duplicate. Access element by index; LinkedList, ArrayList
 * Set - not allow duplicate;
 *      - TreeSet (Contains NavigableMap -> TreeMap)
 *      to do contains TreeSet uses TreeMap.getKey TreeMap uses comparator.
 *      This implementation provides guaranteed log(n) time cost for the basic
 *        operations add, remove and contains.
 *      - HashSet contains HashMap, to do contains uses hash. It makes no guarantees as to the
 *       iteration order of the set. This class offers constant time performance for the basic operations.
 *      - LinkedHashSet- uses LinkedHashMap; store elements in order of insertion, elements are not duplicated.
 *
 * Queue - order by fifo - LinkedList, ArrayDeque
 * Map key-value pairs with no duplicate key allowed
 *
 * Wildcards are used for using polymorphism for generics in collections
 */
@Slf4j
public class Chpt3Collections {

    LinkedHashSet<Integer> sss;

    public static void main(String[] args) {
        Chpt3Collections chpt3Collections = new Chpt3Collections();
       // chpt3Collections.binarySearchTest();
     //  chpt3Collections.wildcardsTest();
        chpt3Collections.testList();
        // chpt3Collections.testSet();
      //  chpt3Collections.testQueue();

    }
    private void testQueue(){
        Consumer<Deque<Integer>> checkQueue = q -> {
            q.push(111);
            q.offer(15); // add to bottom
            q.offer(7);
            q.offerLast(2);
            q.push(19); // add to top
            q.peekFirst();
            q.pop();
            System.out.println(q);
            q.peek(); // retrieve but dont remove
            q.poll(); // retrieve and remove
            System.out.println(q);

        };
        checkQueue.accept(new LinkedList<>()); // list and queue
        checkQueue.accept(new ArrayDeque<>()); // pure queue

    }

    private void testSet(){
        Function<Set<Integer>, Set<Integer>> checkSet = set -> {
            System.out.println(set.getClass());
            set.add(10);
            set.add(7);
            set.add(15);
            set.add(1);
            set.add(2);
            if(set instanceof TreeSet){ // NavigableSet
                System.out.println("lower 7= " + ((TreeSet) set).lower(7));
                System.out.println("floor 6= " + ((TreeSet) set).floor(6));
                System.out.println("higher 6= " + ((TreeSet) set).higher(6));
            }

            return set;
        };
        Set<Integer> hashSetExample = checkSet.apply(new HashSet<>());// any order
        Set<Integer> treeSetExample = checkSet.apply(new TreeSet<>());// should be printed out in ascending order

    }

    private void testList(){

        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("one");
        list.add("two");

    //    System.out.println(list.get(0));
     //   System.out.println(list);
    //    list.remove("one");
//        System.out.println(list.isEmpty());
//        System.out.println(list.size());
//        System.out.println(list.contains("one"));
     //   list.clear();
        System.out.println(list);
        Collections.sort(list, (o1, o2) -> o2.length() - o1.length());
        System.out.println(list);


    }


    private void wildcardsTest() {
        Consumer<List<?>> printArray = System.out::println;
        Consumer<List<? extends Number>> printNumberArray = nums -> {
            for (Number n : nums) {
                System.out.println(n.floatValue());
           }
        };
        Consumer<List<? super String>> printStrArray2 = nums -> {
            for (Object n : nums) {
                System.out.println(n);
            }
        };

        Consumer<List<? super Integer>> printIntArray2 = nums -> {
            for (Object n : nums) {
                System.out.println(n);
            }
        };

        List<String> stringList = new ArrayList<>();
        stringList.add("one");

        List<Integer> intList = new ArrayList<>();
        intList.add(1);

        printArray.accept(stringList);
        printArray.accept(intList);

        printNumberArray.accept(intList);
        printStrArray2.accept(stringList);
        printIntArray2.accept(intList);
    }

    private void binarySearchTest(){
        int [] numbers = {6,9,1,8};
        Arrays.sort(numbers);
        // binarySearch index begins from 0
        System.out.println(Arrays.binarySearch(numbers, 6)); // 1: since index begins from 0
        System.out.println(Arrays.binarySearch(numbers, 3));// -2
        System.out.println(Arrays.binarySearch(numbers, 7)); // -3: -(index where it would be inserted + 1)
    }

    private void test() {

        HashSet<? super ClassCastException> sett = new HashSet<Exception>();

        Set setttt = new HashSet();
        setttt.add(null);
        List<String> llist = new Vector<String>();

        TreeSet<String> tree = new TreeSet<>();
        tree.ceiling("One");

        Map<String, ? super Number> mapp = new HashMap<String, Serializable>();

        mapp.put("x", new Double(0.0));

        mapp.put("y", 15.7);

        mapp.put("z", null);


        mapp.merge("z", 10.0, (a, b) -> a);

        Integer a = 15;
        //   Integer b = a + null;

        new Double(15.7);

        List<String> test = new ArrayList();
        test.add("one");
        test.add("two");
        //  test.add(7);
        test.forEach(log::debug);

        ArrayDeque<?> arrayA = new ArrayDeque<IndexOutOfBoundsException>();

        Map<String, ? super Integer> numericList = new HashMap<>();
        numericList.put("one", 1);
        numericList.put("two", 2);
        Object one = numericList.get("one");
        BiFunction<? super Integer, ? super Integer, ? super Integer> mapper = (v1, v2) -> v1;
        BiFunction<? super Integer, ? super Integer, ? super Integer> biFunction = (v1, v2) -> v1;
        //   numericList.merge("two", 3, biFunction);

        //   Collections.binarySearch(numericList);

        ArrayDeque<String> arrayDeque = new ArrayDeque<>();
        arrayDeque.push("one");
        arrayDeque.offer("end");
        arrayDeque.offerLast("end");

        String pop = arrayDeque.pop();
        String peek = arrayDeque.peek();
        while (arrayDeque.peek() != null) {
            log.info(arrayDeque.pop());
        }
        Deque<String> arrayDeque1 = new LinkedList<>();

        Queue<String> listQueue = new LinkedList<>();
        listQueue.offer("element");
        listQueue.poll();
        listQueue.peek();



        Map<String, Integer> map = new HashMap<>();
        BiConsumer<String, Integer> b1 = map::put;
        BiConsumer<String, Integer> b2 = (k, v) -> map.put(k, v);
        String str2 = "test";
        Predicate<String> isEmpty1 = String::isEmpty;
        //  Predicate<String> isEmpty2 =str2::isEmpty;
        Predicate<String> isEmpty2 = s -> s.isEmpty();
        Predicate<String> isEmpty3 = isEmpty1.and(isEmpty2);
    }

    private GorillaFamily testLiambda(String phoneNumber){
        Gorilla tt = new MyGorilla();

        //        if(isPhoneValid.test(phoneNumber)){
//            StateMachine stateMachine = tt.getStateMachine();
        //      return getStateMachine();
//        } else {
//            throw new RuntimeException();
//        }

        GorillaFamily gorillaFamily = Stream.of(phoneNumber)
                .filter(s -> s.length() == 5)
                .flatMap(s -> Stream.of(tt.getFamily()))
                .findAny()
                .orElseThrow(RuntimeException::new);

        return gorillaFamily;
    }

    interface Gorilla {
        String move();

        default GorillaFamily getFamily() {
            return new GorillaFamily();
        }
    }

    class MyGorilla implements Gorilla {
        @Override
        public String move() {
            return null;
        }
    }

    static class GorillaFamily {
        void playALL() {
            String test = "test";
            play(() -> test);
        }

        void play(Chpt3Collections.Gorilla gorilla) {
            System.out.println(gorilla.move());
        }
    }
}
