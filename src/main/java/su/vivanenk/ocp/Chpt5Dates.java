package su.vivanenk.ocp;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import java.util.ResourceBundle;

@Slf4j
@NoArgsConstructor
public class Chpt5Dates {


    public static void main (String [] args){
      //  Chpt5Dates.testDates();
          Chpt5Dates.testBundles();
    }

    public static void testDates(){

        LocalDate date = LocalDate.now();
        LocalTime time = LocalTime.now();
        System.out.println(date);
        System.out.println(time);
        System.out.println(LocalDateTime.of(date, time));
        ZonedDateTime zoned = ZonedDateTime.now();

        ZoneId zoneId = ZoneId.of("US/Eastern");
        ZonedDateTime zonedDateEngl = ZonedDateTime.now(zoneId);
        ZonedDateTime zonedDateRus = ZonedDateTime.now(ZoneId.systemDefault());



//        ZonedDateTime zonedDateTime2 = ZonedDateTime.of(2022, 6, 15, 15,
//                7, 10, 10, ZoneId.systemDefault());

        log.info("duration = {}", ChronoUnit.HOURS.between(zonedDateRus,zonedDateEngl));


        log.info("duration 1970 = {}", Instant.ofEpochSecond(0));

        LocalDateTime.now().minusYears(1);
        System.out.println(zoned);



        date.minusDays(1).plusDays(5);

        Period period = Period.ofWeeks(1);
        LocalDate plus = date.plus(period);
        System.out.println(Period.ofWeeks(1));
        LocalTime plus1 = time.plus(Duration.ofHours(6));

        StringBuilder sb = new StringBuilder("abcde");
        sb.insert(1, "-").delete(3, 4); //a-bcde.remove =? a-bde
        System.out.println(sb);

        System.out.println(Locale.getDefault());

        LocalDate.of(2018, Month.APRIL, 4);



    }

    public static void testBundles(){
        ResourceBundle zoo = ResourceBundle.getBundle("Zoo");

        ResourceBundle zooFr = ResourceBundle.getBundle("Zoo", Locale.FRANCE);
        ResourceBundle zooSpain = ResourceBundle.getBundle("Zoo", new Locale("ca"));

        log.info("" + Locale.getDefault() + " "+ ZoneId.systemDefault());

        log.info("English zoo name {}", zoo.getString("name"));
        log.info("France zoo name {}", zooFr.getString("name"));
        log.info("Spain zoo name {}", zooSpain.getString("name"));
        log.info("Spain zoo name {}", zooSpain.getString("name2"));

    }

}
