package su.vivanenk.ocp.designpatterns;

public class TravelToPatagonia implements Travel{
    @Override
    public String getCountry() {
        return "Argentina";
    }

    @Override
    public void getVacation() {
        System.out.println("I got big vacation");
    }

    public static void closeHouse(){
        System.out.println("house is closed and keys passed to parents");
    }
}
