package su.vivanenk.ocp.designpatterns;

public class TravelToBelarus implements Travel{
    @Override
    public String getCountry() {
        return "Belarus";
    }

    public static void closeHouse(){
        System.out.println("house is closed");
    }
}
