package su.vivanenk.ocp.designpatterns;

public interface Travel {

    String getCountry();


    default void getVacation(){
        System.out.println("I got two week vacation");
    }
    static void closeHouse(){
        System.out.println("house is closed");
    }

}
