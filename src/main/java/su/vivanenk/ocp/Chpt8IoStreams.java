package su.vivanenk.ocp;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;

@Slf4j
public class Chpt8IoStreams {

    public static void main(String[] args) {
       // Chpt8IoStreams.checkCopy();
       // Chpt8IoStreams.checkBufferedCopy();
        Chpt8IoStreams.copyReaderWriter();

    }

    public static void copyReaderWriter(){
        System.out.format("Hello %1$s", "Test !!!");
       log.info("path {}", Chpt8IoStreams.class.getClassLoader().getResource("application.yml").getPath());
        File source = new File(Chpt8IoStreams.class.getClassLoader().getResource("application.yml").getPath());
        File destination = new File ("application-dev-buffered-writer.yml");



        try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(source), Charset.forName("UTF-8")));
            BufferedWriter writer = new BufferedWriter(new FileWriter(destination))) {

            String str;

            while ((str= reader.readLine()) != null) {
                writer.write(str);
                writer.newLine();
            }

        } catch (IOException ex){
            log.error("exception", ex);
        }

    }

    public static void checkBufferedCopy(){
        log.info("path {}", Chpt8IoStreams.class.getClassLoader().getResource("application.yml").getPath());
        File source = new File(Chpt8IoStreams.class.getClassLoader().getResource("application.yml").getPath());
        File destination = new File ("application-dev-buffered.yml");
        try(InputStream bis = new BufferedInputStream(new FileInputStream(source));
            OutputStream bos = new BufferedOutputStream(new FileOutputStream(destination))){
            int length;
            byte [] buffer = new byte[1024];
            while ( (length = bis.read(buffer)) >0 ){
                bos.write(buffer, 0 , length);
                bos.flush();
            }

        } catch (IOException ex){
            log.error("exception", ex);
        }
    }


    public static void checkCopy(){
        log.info("path {}", Chpt8IoStreams.class.getClassLoader().getResource("application.yml").getPath());
        File source = new File(Chpt8IoStreams.class.getClassLoader().getResource("application.yml").getPath());
        File destination = new File ("application-dev.yml");

        try(InputStream is = new FileInputStream(source);
            OutputStream os = new FileOutputStream(destination)) {
            int b;
            while((b = is.read()) != -1){
                os.write(b);
            }
        } catch (IOException ex) {
            log.error("exception ", ex);
        }
    }
}
