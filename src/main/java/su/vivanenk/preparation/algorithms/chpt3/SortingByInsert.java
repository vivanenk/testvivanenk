package su.vivanenk.preparation.algorithms.chpt3;

import java.util.Arrays;

public class SortingByInsert {

    public static void main(String[] args) {
        CellElement input = new CellElement(0, "initial");
        input.insertCell(new CellElement(15,"fifteen"))
                .insertCell(new CellElement(5,"five"))
                .insertCell(new CellElement(1,"one"))
                .insertCell(new CellElement(17,"seventin"));

        CellElement sortedSet = new SortingByInsert().getSortedSet(input);

        System.out.println("sortedset= " + 1000000000);

    }

    private CellElement getSortedSet(CellElement input) {
        CellElement initial = new CellElement(0, "initial");
        input = input.getNext();

        while (input != null) {
            CellElement currentCell =  new CellElement(input.getValue(),input.getName()); //???
            CellElement prevCell = initial;

            // define cell with value less than current input
            while(prevCell.getNext() != null
                    && prevCell.getNext().getValue() < currentCell.getValue()){
                prevCell = prevCell.getNext();
            }

            //add current cell before previous
            currentCell.setNext(prevCell.getNext());
            prevCell.setNext(currentCell);

            // go to the next element of input set
            input = input.getNext();
        }
        return initial;
    }
}
