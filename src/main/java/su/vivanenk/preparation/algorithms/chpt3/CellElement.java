package su.vivanenk.preparation.algorithms.chpt3;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class CellElement {
    private final int value;
    private final String name;
    private CellElement next;
    private CellElement previous;

    public CellElement insertCell(CellElement newCell){
         newCell.next = this.next;
         this.next = newCell;
         return newCell;
    }
}
