package su.vivanenk.preparation.leetcode;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class MergeSortedList {

    public static void main(String[] args) {
        ListNode listNode1 =  new ListNode(1);
        listNode1.next = (new ListNode(3));
        listNode1.next.next = (new ListNode(4));
        log.info("first list = {}", listNode1);

        ListNode listNode2 =  new ListNode(1);
        listNode2.next = (new ListNode(2));
        listNode2.next.next = (new ListNode(4));
        log.info("second list = {}", listNode2);

       // log.info("merged list = {}", new MergeSortedList().mergeTwoLists(listNode1, listNode2));

        log.info("merged list2 = {}", new MergeSortedList().mergeTwoLists2(listNode1, listNode2));

    }


 // Definition for singly-linked list.
  public static class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }

     @Override
     public String toString(){
          ListNode toNext = this.next;
          StringBuilder sb = new StringBuilder();
          sb.append(this.val).append(", ");
          while (toNext != null){
              sb.append(toNext.val).append(", ");
              toNext = toNext.next;
          }
          return sb.toString();
     }
  }

    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        List<Integer> list = new ArrayList<>();
        while(list1 != null){
            list.add(list1.val);
            list1 = list1.next;
        }
        while(list2 != null){
            list.add(list2.val);
            list2 = list2.next;
        }
        list.sort(Comparator.naturalOrder());
        log.info("list={}", list);
        List<ListNode> collect = list.stream().map(ListNode::new).collect(Collectors.toList());
        for (int i =0 ; i< collect.size()-1; i++){
            collect.get(i).next = collect.get(i+1);
        }
        return collect.get(0);
    }

    public ListNode mergeTwoLists2(ListNode list1, ListNode list2) {
        ListNode head = new ListNode();
        ListNode tail = head;
        while (list1 != null || list2 != null) {
            if (list1 == null || (list2 != null && list2.val < list1.val)) {
                tail.next = list2;
                tail = tail.next;
                list2 = list2.next;
            } else {
                tail.next = list1;
                tail = tail.next;
                list1 = list1.next;
            }
        }
        return head.next;
    }
}
