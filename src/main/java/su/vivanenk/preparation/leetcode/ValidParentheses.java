package su.vivanenk.preparation.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidParentheses {
    // (()[])
    static Map<Character, Character> characters;
    static {
        characters = new HashMap<>();
        characters.put(')', '(');
        characters.put('}', '{');
        characters.put(']', '[');

    }

    public static void main(String[] args) {
        System.out.println("result = " + new ValidParentheses().isValid("({[]})")); //({[}])
    }

    public boolean isValid(String s) {
        return validate(s);
    }

    private boolean validate(String s) {

        while (s.length() > 0) {
            int endIndex = 0;
            if (s.charAt(0) == '(') endIndex = s.indexOf(')') + 1;
            if (s.charAt(0) == '{') endIndex = s.indexOf('}') + 1;
            if (s.charAt(0) == '[') endIndex = s.indexOf(']') + 1;

            if (endIndex <= 0) return false;

            char[] chars = s.substring(0, endIndex).toCharArray();
            boolean b = validatePart(chars);
            if (!b) {
                return false;
            } else {
                return validate(s.substring(endIndex));
            }
        }
        return true;

    }

    private boolean validatePart(char[] chars) {
        int countOne = 0;
        int countTwo = 0;
        int countThree = 0;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '(') countOne++;
            if (chars[i] == ')') countOne--;

            if (chars[i] == '{') countTwo++;
            if (chars[i] == '}') countTwo--;

            if (chars[i] == '[') countThree++;
            if (chars[i] == ']') countThree--;

            if(characters.get(chars[i])!=null && characters.get(chars[i]) != chars[i-1]) return false;

        }
        return (countOne == 0 && countTwo == 0 && countThree == 0);

    }
}
