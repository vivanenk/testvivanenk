package su.vivanenk.preparation.leetcode;

import java.util.ArrayList;
import java.util.List;

public class Palindrome {

        List<Integer> numberByRank = new ArrayList<>();

    public static void main(String[] args) {
        System.out.println(new Palindrome().isPalindrome(1211));
        System.out.println(new Palindrome().isPalindrome2(585));
    }

        public boolean isPalindrome(int x) {
            if (x < 0) return false;
            x = fillDemantion(x, 1000000000);
            x = fillDemantion(x, 100000000);
            x = fillDemantion(x, 10000000)  ;
            x = fillDemantion(x, 1000000)  ;
            x = fillDemantion(x, 100000)  ;
            x = fillDemantion(x, 10000)  ;
            x = fillDemantion(x, 1000)  ;
            x = fillDemantion(x, 100) ;
            x = fillDemantion(x, 10);
            x = fillDemantion(x, 1);

            System.out.println("map = " + numberByRank);

            List <Integer>corrected = new ArrayList<>();
            int size =  numberByRank.size();

            boolean pass = true;

            for (int i = 0 ; i < size ; i ++){
                if (pass && numberByRank.get(i) == 0){
                    continue;
                } else {
                    pass = false;
                    corrected.add(numberByRank.get(i));
                }
            }

            for (int i =0 ;i< corrected.size(); i++) {
                if (corrected.get(i) != corrected.get(corrected.size() -1 -i)) return false;
            }


            return true;
        }

        private int fillDemantion(int x, int rank){
            int i = x / rank;
            if (i >= 0) {
                numberByRank.add(i);
                return x - i * rank;
            }
            return x;
        }


    public boolean isPalindrome2(int x) {
        if(x<0)
            return false;
        int temp = x;
        int rem,rev=0;
        // reversed integer is stored in variable
        while( x != 0 )
        {
            rem= x % 10;
            rev= rev * 10 + rem;
            x=x/10;
        }

        // palindrome if orignalInteger(temp) and reversedInteger(rev) are equal
        if (temp == rev)
            return true;
        return false;
    }

}
