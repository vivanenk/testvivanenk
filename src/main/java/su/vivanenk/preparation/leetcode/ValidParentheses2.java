package su.vivanenk.preparation.leetcode;

import java.util.*;

public class ValidParentheses2 {

    public static void main(String[] args) {
        System.out.println("result = " + new ValidParentheses2().isValid("({[]}){{}}")); //({[}])
    }

    public boolean isValid(String s) {
        Map<Character, Character> characters = new HashMap<>();
        characters.put(')', '(');
        characters.put('}', '{');
        characters.put(']', '[');

        Deque<Character> array = new ArrayDeque<>();
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
           if(chars[i]=='(' || chars[i] == '{' || chars[i] == '[') {
               array.add(chars[i]);
           } else {
               if (characters.get(chars[i]) != array.peekLast()){
                   return false;
               } else {
                   array.pollLast();
               }
           }
        }
        return array.isEmpty();
    }
}
