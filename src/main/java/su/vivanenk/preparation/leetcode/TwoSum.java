package su.vivanenk.preparation.leetcode;

import java.util.HashMap;
import java.util.Map;

public class TwoSum {
    public static void main(String[] args) {
        new TwoSum().new Solution().twoSum(new int[]{2, 7, 11, 15}, 9);
        new TwoSum().new Solution().twoSum2(new int[]{2, 7, 11, 15}, 9);
    }

    class Solution {
        public int[] twoSum(int[] nums, int target) {
            for (int i = 0; i < nums.length; i++) {
                int index = getIndex(i, nums, target);
                if (index > -1) {
                    return new int[]{i, index};
                }
            }
            return null;
        }

        private int getIndex(int index, int[] nums, int target) {
            int currentElem = nums[index];
            for (int i = 0; i < nums.length; i++) {
                if (i == index) continue;
                if (currentElem + nums[i] == target) {
                    return i;
                }
            }
            return -1;
        }

        public int[] twoSum2(int[] nums, int target) {
            int[] res = new int[2];
            Map<Integer, Integer> map = new HashMap<>();
            for (int i = 0; i < nums.length; i++) {
                if (map.containsKey(target - nums[i])) {
                    res[1] = i;
                    res[0] = map.get(target - nums[i]);
                    return res;
                }
                map.put(nums[i], i);
            }
            return res;
        }

    }

}
