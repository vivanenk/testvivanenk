package su.vivanenk.preparation.leetcode;

import java.util.*;

public class RomeToInt {

    public static void main(String[] args) {
        new RomeToInt().new Solution().romanToInt("CLIX");
        new RomeToInt().new Solution().romanToInt2("CLIX");
    }

    class Solution {
        public int romanToInt(String s) {
            Map<String, Integer> sumMap = new HashMap<>();
            sumMap.put("I", 1);
            sumMap.put("V", 5);
            sumMap.put("IV", 4);
            sumMap.put("X", 10);
            sumMap.put("IX", 9);
            sumMap.put("L", 50);
            sumMap.put("XL", 40);
            sumMap.put("C", 100);
            sumMap.put("XC", 90);
            sumMap.put("D", 500);
            sumMap.put("CD", 400);
            sumMap.put("M", 1000);
            sumMap.put("CM", 900);

            int result = 0;
            char[] chars = s.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                String currentChar = Character.toString(chars[i]);
                if ((i + 1) < chars.length) {
                    String currentAndNext= currentChar + chars[i + 1];
                    Integer summa = sumMap.get(currentAndNext);
                    if (summa != null){
                        result += summa;
                        i++;
                    } else {
                        result += sumMap.get(currentChar);
                    }
                } else {
                    result += sumMap.get(currentChar);
                }
            }
            System.out.println(result);
            return result;
        }

        public int romanToInt2(String s) {
            Map<Character,Integer>map=new HashMap<>();
            map.put('I', 1);
            map.put('V', 5);
            map.put('X', 10);
            map.put('L', 50);
            map.put('C', 100);
            map.put('D', 500);
            map.put('M', 1000);

            int result=map.get(s.charAt(s.length()-1));

            for(int i=s.length()-2;i>=0;i--){
                if(map.get(s.charAt(i))<map.get(s.charAt(i+1))){
                    result-=map.get(s.charAt(i));
                }else{
                    result+=map.get(s.charAt(i));
                }
            }
            System.out.println(result);
            return result;
        }
    }
}
