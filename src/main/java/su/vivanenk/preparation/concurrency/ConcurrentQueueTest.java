package su.vivanenk.preparation.concurrency;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

/**
 * Producer-Consumer pattern
 *
 *  - How to crate Runnable task with incoming data ?
 *   - How to correct use parametrised bounds ?
 *   - How to use blockedqueue and emulate several threads' cooperation ?
 */

@Slf4j
public class ConcurrentQueueTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ConcurrentQueueTest concurrentQueueTest = new ConcurrentQueueTest();
        concurrentQueueTest.testBlockingQueue();
    }

    public void testBlockingQueue() throws ExecutionException, InterruptedException {
      //  SynchronousQueue<MutableObject> queue = new SynchronousQueue<>();
        LinkedBlockingQueue<MutableObject> queue = new LinkedBlockingQueue<>();

        DataStorage dataStorage = new DataStorage(queue);

        ProducerTask producerTask = new ProducerTask();
        producerTask.setDataStorage(dataStorage);
        producerTask.setData(new MutableObject("Valia", 15));

        ConsumerTask<MutableObject> consumerTask = new ConsumerTask<>();
        consumerTask.setDataStorage(dataStorage);

        ExecutorService executorService = null;
        try {
            executorService = Executors.newFixedThreadPool(3); // is it correct? it is not single thread executor

            executorService.submit(producerTask);
            executorService.submit(producerTask);
            Future<MutableObject> submit = executorService.submit(consumerTask);
            MutableObject mutableObject = submit.get();
            executorService.submit(producerTask);
        } finally {
            if (executorService != null) executorService.shutdown();
        }
    }

    @Getter
    @Setter
    private static final class ProducerTask implements Runnable {
        private DataStorage dataStorage;
        private MutableObject data;

        @Override
        public void run() {
            try {
                dataStorage.addDataIntoQueue(data);
            } catch (InterruptedException e) {
                log.error("error in producer", e);
                Thread.currentThread().interrupt();
            }
        }
    }
    @Getter
    @Setter
    private static final class ConsumerTask <T extends MutableObject> implements Callable<T> {
        private DataStorage dataStorage;

        @Override
        public T call()  {
            try {
                return  (T) dataStorage.getDataFromQueue();
            } catch (InterruptedException e) {
                log.error("error in consumer", e);
                Thread.currentThread().interrupt(); //???
            }
            return null;
        }
    }

}
