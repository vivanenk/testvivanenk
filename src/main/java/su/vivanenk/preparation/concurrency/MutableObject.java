package su.vivanenk.preparation.concurrency;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class MutableObject {
    private String name;
    private int age;
}
