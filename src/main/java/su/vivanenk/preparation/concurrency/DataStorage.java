package su.vivanenk.preparation.concurrency;

import java.util.concurrent.BlockingQueue;

public class DataStorage {
    private final BlockingQueue<MutableObject> blockingQueue;

    public DataStorage(BlockingQueue<MutableObject> queue){
        this.blockingQueue = queue;
    }

    public void addDataIntoQueue(MutableObject data) throws InterruptedException {
        System.out.println("put data");
        blockingQueue.put(data);
        System.out.println(" data putted");
    }

    public MutableObject getDataFromQueue() throws InterruptedException {
        System.out.println("take data");
        MutableObject take = blockingQueue.take();
        System.out.println(" data taken");
        return take;
    }

}
