package su.vivanenk.preparation.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@SpringBootApplication
@Configuration
public class Question1 {
    @Autowired
    private static Service service;

//     public static void main(String[] args) {
//         SpringApplication.run(Question1.class, args);
//    }
    @Bean
    public MyBean initBean(Service service){
        System.out.println("service is " + service);
         return new MyBean();
    }


    @Component
    class Service {

    }

    class MyBean{
        Service service;

    }
}
