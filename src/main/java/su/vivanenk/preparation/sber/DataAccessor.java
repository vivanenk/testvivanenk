package su.vivanenk.preparation.sber;

import lombok.RequiredArgsConstructor;

import java.io.*;

/**
 * This class is thread safe.
 */
@RequiredArgsConstructor
public class DataAccessor {
    private final File sourceFile;

    public String getContent() throws IOException {
        StringBuffer stringBuffer = new StringBuffer();
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(sourceFile)))){
            String str;
            while ((str = reader.readLine()) != null){
                stringBuffer.append(str);
            }
            return stringBuffer.toString();
        }
    }

    public String getContentWithoutUnicode() throws IOException {
        try(InputStream i = new FileInputStream(sourceFile)) {
            String output = "";
            int data;
            while ((data = i.read()) > 0) {
                if (data < 0x80) {
                    output += (char) data;
                }
            }
            return output;
        }
    }

    public void saveContent(String content) throws IOException {
        try(OutputStream out = new BufferedOutputStream(new FileOutputStream(sourceFile))){
            out.write(content.getBytes());
        }
    }

}
