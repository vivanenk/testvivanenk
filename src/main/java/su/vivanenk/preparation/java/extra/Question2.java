package su.vivanenk.preparation.java.extra;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Question2 {

    public static void main(String[] args) {
        new Question2().checkOne();
        new Question2().checkTwo();

    }

    public void checkOne() {
        List<Integer> list = List.of(1,2,3,4);
        int total =0;
        for(Integer elem : list){
            if (elem % 2 == 0) {
                total += elem * elem;
            }
        }
        System.out.println(total);
    }
    public void checkTwo() {
        int total = Stream.of(1, 2, 3, 4)
                .filter(x -> x % 2 == 0)
                //.mapToInt(x -> x * x)
               // .sum();
                //  .map(x -> x * x)
               //  .reduce(0, (x, y) -> x + y);
               .reduce(2, (x, y) -> x * y);
        System.out.println(total);
    }
}
