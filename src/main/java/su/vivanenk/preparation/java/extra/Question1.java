package su.vivanenk.preparation.java.extra;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Question1 {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("Apples", 3);
        map.put("Oranges",2);
        int apples = map.get("Apples");
        map.put("Apples", apples + 4);
        System.out.println(map.get("Apples"));
    }
}
