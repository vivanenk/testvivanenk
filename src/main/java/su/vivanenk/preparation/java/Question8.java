package su.vivanenk.preparation.java;

public class Question8 {

    public enum Dirrection {
        EAST("E"),
        WEST("W");

        private final String shortCode;

        Dirrection(String shortCode) {
            this.shortCode = shortCode;
        }
    }
}
