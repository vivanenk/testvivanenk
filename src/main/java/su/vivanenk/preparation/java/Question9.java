package su.vivanenk.preparation.java;

import java.util.function.Function;

public class Question9 {

    static Function<Integer, Integer> sq = x -> x * x;

    public static void main(String[] args) {
        int square = new Question9().square(5);
        int square2 = sq.apply(5);
        System.out.println(square);
        System.out.println(square2);
    }

    public int square(int x){
        return x *x;
    }
}
