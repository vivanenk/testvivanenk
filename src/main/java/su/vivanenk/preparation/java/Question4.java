package su.vivanenk.preparation.java;

public class Question4 {
    String berry = "blue";

    public static void main(String[] args) {
        new Question4().juicy("straw");
    }

    void juicy(String berry){
        this.berry = "rasp";
        System.out.println(berry + "berry");
    }
}
