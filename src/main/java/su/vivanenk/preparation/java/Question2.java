package su.vivanenk.preparation.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class Question2 {

    public static void main(String[] args) {
        check();
    }

    public static void check(){
        String [] array = new String[]{"A", "B", "C"};

        List<String>list1 = Arrays.asList(array);
        List<String> list2= new ArrayList<>(Arrays.asList(array));
        List<String> list3= new ArrayList<>(Arrays.asList("A", new String("B"), "C"));
        System.out.println(list1.equals(list2));
        boolean equals = list1.equals(list3);
        System.out.println(equals);
    }

}
