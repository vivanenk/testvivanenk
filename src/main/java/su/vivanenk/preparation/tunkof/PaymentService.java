package su.vivanenk.preparation.tunkof;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Currency;

/**
 * Компонент проведения платежей.
 * <p>
 * Платеж - это сумма в валюте, которая переводится от одного клиента другому.
 * <p>
 * Сумма платежа при сохранении должна быть пересчитана в рубли по курсу ЦБ на текущую дату.
 * <p>
 * При платеже должна быть выставлена комиссия, которая зависит от суммы платежа.
 * <p>
 * После платежа надо вызвать сервис нотификаций, который отправит уведомления пользователям --
 * для клиентов это будет выглядеть как push уведомление.
 * <p>
 * Компонент переводит деньги от авторизованного пользователя переданному на вход.
 */
@Service
public class PaymentService {
//    @Autowired
//    private PaymentRepository paymentRepository;
//    @Autowired
//    private FeeRepository feeRepository;
//    @Autowired
//    private UserRepository userRepository;
//
//    private NotificationRestClient notificationRestClient = new NotificationRestClient();
//    private CbrRestClient cbrRestClient = new CbrRestClient();

    @Transactional
    public void processPayment(double amount, Currency currency, Long recipientId) {
//        double amountInRub = amount * cbrRestClient.doRequest().getRates().get(currency.getCode());
//        Long userId = (Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        User user = userRepository.findUserById(userId).get();
//        Payment payment = new Payment(amountInRub, user, recipientId);
//        paymentRepository.save(payment);
//        if (amountInRub < 1000) {
//            Fee fee = new Fee(amountInRub * 0.015, user);
//            feeRepository.save(fee);
//        }
//        if (amountInRub > 1000) {
//            Fee fee = new Fee(amountInRub * 0.01, user);
//            feeRepository.save(fee);
//        }
//        if (amountInRub > 5000) {
//            Fee fee = new Fee(amountInRub * 0.005, user);
//            feeRepository.save(fee);
//        }
//        try {
//            notificationRestClient.notify(payment);
//        } catch (Throwable t) {
//            // do nothing
//        }
    }
}