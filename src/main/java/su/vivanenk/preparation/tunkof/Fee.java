package su.vivanenk.preparation.tunkof;

import org.springframework.context.annotation.Configuration;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
class Fee {
    private Double value;

    @Id
    private Long id;

   // @ManyToOne
    private User user;

    public Fee() {
    }

    public Fee(Double value, User user) {
        this.value = value;
        this.user = user;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public void setUser(User user) {
        this.user = user;
    }

    private class User{}


}
