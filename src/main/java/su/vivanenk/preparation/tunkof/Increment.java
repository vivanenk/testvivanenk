package su.vivanenk.preparation.tunkof;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

public class Increment {
    private static AtomicInteger counter1 = new AtomicInteger();
    private static AtomicInteger counter2 = new AtomicInteger();

    public static void main(String[] args) throws InterruptedException {
        int tasksCount = 100000;
        ExecutorService executor = null;
        try {
            CountDownLatch latch = new CountDownLatch(tasksCount);
            executor = Executors.newFixedThreadPool(100);
            for (int i = 0; i < tasksCount; i++) {
                executor.submit(() -> {
                    try {
                        counter1.incrementAndGet();
                        counter2.incrementAndGet();
                    } finally {
                        latch.countDown();
                    }
                });
            }
            latch.await();

            System.out.println(counter1);
            System.out.println(counter2);
            System.exit(0);
        } finally {
           if (executor != null) executor.shutdown();
        }
    }
}