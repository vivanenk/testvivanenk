package su.vivanenk.preparation;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test {

    int[] array1 = {2,2,3,5,7,6,4};
    int[] array2 = {1,2,3,4,6,7};

    public static void main(String[] args) {
        Test test = new Test();
        test.printCrossValues();
    }

    public void printCrossValues() {
        String test = "test";
        System.out.println("char=" + test.charAt(1));
        Set<Integer> result = new HashSet<>();

        Set<Integer> arrayOne = Arrays.stream(array1).boxed().collect(Collectors.toSet());
        Set<Integer> arrayTwo = Arrays.stream(array2).boxed().collect(Collectors.toSet());

        System.out.println(Arrays.stream(this.array1)
                .filter(i -> Arrays.asList(this.array2).contains(i)).distinct()
                .peek(System.out::println)
                .boxed()
                .collect(Collectors.toList()));

        for(Integer element: arrayTwo){
            if(arrayOne.contains(element)) result.add(element);
        }
        System.out.println("Crossed elements:" + result);
    }
}
