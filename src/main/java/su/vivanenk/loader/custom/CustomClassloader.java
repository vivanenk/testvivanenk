package su.vivanenk.loader.custom;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class CustomClassloader extends ClassLoader {

    private byte[] bytes;

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        Class<?> clazz = findLoadedClass(name);
        if (clazz != null) {
            return clazz;
        }

        clazz = findClass(name);

        return clazz;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        byte[] bytesLoaded = null;
        try {
            bytesLoaded = loadClassData(name);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (bytesLoaded == null) {
            throw new ClassNotFoundException(name);
        }

        Class<?> clazz = defineClass(name, bytesLoaded, 0, bytesLoaded.length);
        if (clazz == null) {
            throw new ClassNotFoundException(name);
        }

        return clazz;
    }

    private byte[] loadClassData(String name) throws IOException,
            ClassNotFoundException {
        final String fullPath = name.replace('.', '/') + ".class";
        InputStream is = getResourceAsStream(fullPath);
        if (is == null) {
            throw new ClassNotFoundException(name);
        }
        bytes = is.readAllBytes();
        is.close();
//        byte buf[] = new byte[1024];
//        DataInputStream dis = new DataInputStream(is);
//        dis.read(buf);
//        dis.close();

        return bytes;
    }
}
