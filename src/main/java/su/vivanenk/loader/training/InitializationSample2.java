package su.vivanenk.loader.training;

class Super2 {

    static final int i = 1;
    static final String s = "s";

    static {
        System.out.println(Super2.class.getName());
    }
}

class Sub2 extends Super2 {

    static {
        System.out.println(Sub2.class.getName());
    }
}

public class InitializationSample2 {

    public static void main(String[] args) {
        System.out.println(Sub2.i);
        System.out.println(Sub2.s);
    }
}



