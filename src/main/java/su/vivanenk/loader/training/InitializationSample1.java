package su.vivanenk.loader.training;

class One {
	static { System.out.print("One "); }
}

class Super {
	static { System.out.print("Super "); }
}

class Sub extends Super {
	static { System.out.print("Sub "); }
}

public class InitializationSample1 {

    public static void main(String[] args) {
        One o = null;
        Sub sub = new Sub();
    }
}


