package su.vivanenk.loader.training;

abstract class Super3 {

    public Super3() {
        setI(10);
    }

    protected abstract void setI(int i);
}


class Sub3 extends Super3 {

    protected int i;

    public Sub3() {
    }

    @Override
    protected void setI(int i) {
        this.i = i;
    }
}

public class InitializationSample3 {

    public static void main(String args[]) {
        System.out.println(new Sub3().i);
    }
}
