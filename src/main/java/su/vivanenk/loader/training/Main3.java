package su.vivanenk.loader.training;

import su.vivanenk.loader.custom.CustomClassloader;

public class Main3 {

    public static void main(String[] args) throws ClassNotFoundException {
        Integer integer = new Integer(1);
        System.out.println(integer);
        CustomClassloader classloader = new CustomClassloader();
       // Class<?> clazz = classloader.loadClass("java.lang.Integer");
        Class<?> clazz = classloader.loadClass("su.vivanenk.loader.training.DbUtils");
        System.out.println(clazz.getClassLoader());
    }
}


