# Description

This test project is about devops school home works implementation. 

---

## Home work1

 1. Register yourself at https://gitlab.com 
 2. Create a new project 
 3. Create a pipeline with 3 stages: build – test – deploy 
 4. Deploy should have two manual jobs “dev” and “prod”
![](./cicd1.png)

## Home work 2 
1. Add building of any application in pipeline.
2. Add docker tags for jobs as anchor.
3. Add application from “build” to artifacts with expiration in 1 hour.
4. Add a stage and job to dockerize application from artifacts.
5. Push resulted docker image to any container registry with pipeline number tag.
6. Add artifacts for “test” with expiration in one day.

![](./cicd2.png)

## Home work 3 
- Add cache for build job. 

![](./cicd3.png)

## Home work 4
1. Add “dev” and “prod” environments. 
2. Add any urls for both environments.

![](./cicd4.png)
